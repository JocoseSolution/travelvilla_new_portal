﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb"
    Inherits="UserControl_LoginControl" %>
<link href="icofont/icofont.css" rel="stylesheet" />
<link href="icofont/icofont.min.css" rel="stylesheet" />


<style type="text/css">
    .trav-b2b {
        font-size: 20px;
    color: #000000;
    width: 800px;
    margin: 0 auto;
    text-align: center;
    font-weight: 600;
    padding: 0px 0 60px 0;
    }

    .b2b-product-tab {
        display: flex;
    justify-content: center;
    align-items: center;
    padding-bottom: 150px;
    /* max-width: 500px; */
    margin: 0 auto;
    margin-right: 50%;
    margin-left: 55%;
}

    .product-type {
    height: 100px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-end;
    padding-right: 93px;
}

    .b2b-static-cont .b2b-product-tab .product-type img {
    display: block;
}

   .product-type span {
    display: block;
    font-size: 22px;
    color: #ee3157;
    padding-top: 17px;
    font-weight: 600;
}

   .easier-management-types-cont {
    background: #fafafa;
    margin-bottom: 90px;
}

   .easier-management-types-cont .perfect-heading {
    padding-top: 100px;
    padding-bottom: 60px;
}

   .aertrip-color {
    color: #ee3157;
}

   .easier-management-types {
       display: flex;
    justify-content: center;
    text-align: center;
    /*margin-left: 50%;
    margin-right: 46%;*/
}

   .easier-management-types .management-lists {
    margin-right: 50px;
    width: 162px;
    color: #000;
    font-size: 16px;
    font-weight: 600;
}

   .easier-management-types .management-lists .image {
    padding: 0;
    width: 93px;
    margin: 0 auto;
}

   .easier-management-types .management-lists .name {
    padding-top: 16px;
    color: #000;
}

   
element.style {
}
.easier-management-para {
    font-size: 24px;
    padding: 85px 0 100px 0;
    width: 916px;
    margin: 0 auto;
    text-align: center;
    color: #000000;
}
.perfect-heading {
    font-size: 55px;
    text-align: center;
    margin: 0;
    padding: 0 0 50px 0;
    font-weight: bold;
    color: #2c3a4e;
}

</style>

<asp:Login ID="UserLogin" runat="server">


    <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>


        

        
<%--<section>
		<div class="tr-register">
			<div class="tr-regi-form">
				<h4 style="color:#eee;">Sign In</h4>
				
				<form class="col s12">
					<div class="row">
						<div class="input-field col s12">
							<asp:TextBox runat="server" ID="UserName" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
							<label class="">User Name</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<asp:TextBox ID="Password"  TextMode="Password" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
							<label class="">Password</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
							<i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style="">
                                <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="waves-button-input"/>
                                <span class=""></span>

							</i> </div>
					</div>
				</form>
				<p><a href="../ForgotPassword.aspx">forgot password</a> | Are you a new user ? <a href="../regs_new.aspx">Register</a>
				</p>
				
			</div>
		</div>
	</section>--%>


           <div class="theme-hero-area" style="margin-top:62px;">
      <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-strong" style="background: #fff !important;"></div>
      </div>
      <div class="theme-hero-area-body">
        <div class="theme-page-section _pt-100 theme-page-section-xl" style="padding-top: 50px !important;">
          <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <%--<img src="../Advance_CSS/24349364.jpg" style="margin-top: -117px;margin-left: -150px;"/>--%>
                    
                    <img src="Advance_CSS/PngItem_5186444.png" style="width:71%;"/>
                </div>
              <div class="col-md-4">
                <div class="theme-login theme-login-white">
                  <div class="theme-login-header">
                    <h1 class="theme-login-title">A Destination For The New Millennium</h1>
                   
                  </div>
                  <div class="">
                    <div class="theme-login-box-inner">
                      <form class="theme-login-form">


                          <div class="form-group theme-login-form-group">
                                <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                                
                                    <div class="">
                                        <i class="theme-search-area-section-icon lin lin-user" style="line-height: 44px !important;"></i>
                                        <asp:TextBox runat="server"  class="theme-search-area-section-input typeahead" placeholder="User Id" ID="UserName" style="height: 45px !important;border-radius:5px;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>



                                <div class="form-group theme-login-form-group">
                                <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-bg-white theme-search-area-section-no-border theme-search-area-section-mr">
                                   
                                    <div class="">
                                        <i class="theme-search-area-section-icon lin lin-lock" style="line-height: 44px !important;"></i>
                                        <asp:TextBox runat="server"  class="theme-search-area-section-input typeahead" TextMode="Password" placeholder="Password" ID="Password" style="height: 45px !important;border-radius:5px;"></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>


   
                          <div class="row">
                              <div class="col-md-6">
                                  <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-uc btn-dark btn-block btn-lg-custom"/>
                              </div>

                              <div class="col-md-6">
                          <a href="ForgotPassword.aspx">Forgot Password ?</a>
                                  </div>
                              </div>
                      </form>
                     
                      
                    </div>
                    <div class="theme-login-box-alt">
                      <p>Not yet registered? &nbsp;
                        <a href="regs_new.aspx" style="color:#ee3157;">Register Here</a>
                      </p>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        <div class="trav-b2b">Travelvilla is the world's Smartest B2B Travel Platform, giving travel agents more than 50 exclusive features to transact faster and earn more on Flights, Hotels, Holidays and Visas. See why travel agents are putting Travelvilla to work...</div>
        <div class="b2b-product-tab">
              <div class="product-type">
                  <i class="icofont-airplane icofont-4x" style="color:#ee3157;"></i>
                  <span>Flights</span>
              </div>

              <div class="product-type">
                  <i class="icofont-hotel icofont-4x" style="color:#ee3157;"></i>
                  <span>Hotels</span>
              </div>

              <div class="product-type">
                  <i class="icofont-beach icofont-4x" style="color:#ee3157;"></i>
                  <span>Holidays</span>
              </div>

              <div class="product-type">
                  <i class="icofont-visa-alt icofont-4x" style="color:#ee3157;"></i>
                  <span>Visas</span>
              </div>
          </div>


        <div class="easier-management-types-cont">
          <h1 class="perfect-heading"><span class="aertrip-color">Easier</span> Management</h1>
          <div class="easier-management-types">
              <div class="management-lists">
                  <div class="image"><img src="Advance_CSS/Icons/website.png" /></div>
                  <div class="name">Your very own CRM</div>
              </div>
              <div class="management-lists">
                  <div class="image"><img src="Advance_CSS/Icons/credit-card-payment.png" /></div>
                  <div class="name">Multiple Payment options</div>
              </div>
              <div class="management-lists">
                  <div class="image"><img src="Advance_CSS/Icons/booking.png" /></div>
                  <div class="name">Online booking management</div>
              </div>
              <div class="management-lists">
                      <div class="image"><img src="Advance_CSS/Icons/customer-service.png" /></div>
                      <div class="name">Online after sales</div>
                  </div>
              <div class="management-lists">
                  <div class="image"><img src="Advance_CSS/Icons/accounting.png" /></div>
                  <div class="name">Online Accounting</div>
              </div>
          </div>
          <div class="easier-management-para">And that’s not all! A lot more features like Timeline view, Filter Graphs, Group Booking tools are available online. This with unparalleled Security &amp; Privacy and 24/7 Online &amp; Offline Support makes Travelvilla the best B2B travel platform out there.</div>
      </div>


        <div class="large-12 medium-12 small-12" style="display:none;">
            <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
            <div class="lft f16" style="display: none;">

                Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
            </div>
            <%--<div class="rgt">
                <a href="ForgotPassword.aspx" rel="lyteframe" class="forgot">Forgot Your password (?)</a>
                </div>--%>
            <div class="clear1">
            </div>

            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    <%--<asp:TextBox ID="UserName" class="form-controlsl " BackColor="White" placeholder="Enter Your User Id" runat="server"></asp:TextBox>--%>
                </div>
                <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="    font-size: 23px;" ></i></span>
                   <%-- <asp:TextBox ID="Password" class="form-controlsl" BackColor="White" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>--%>
                </div>
               <%-- <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
                <%--<asp:Button ID="LoginButton" runat="server" ValidationGroup="UserLogin" OnClick="LoginButton_Click" CssClass="btnsss"
                    Text="LOGIN" />--%>
                <br />

                <a href="../ForgotPassword.aspx" style="color:#ff0000">Forgot Password</a>
            </div>
            <div class="clear">
            </div>
            <div>
                <asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="clear">
            </div>
        </div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />     
</asp:Login>



<script>
    var overlay = document.getElementById("overlay");

    // Buttons to 'switch' the page
    var openSignUpButton = document.getElementById("slide-left-button");
    var openSignInButton = document.getElementById("slide-right-button");

    // The sidebars
    var leftText = document.getElementById("sign-in");
    var rightText = document.getElementById("sign-up");

    // The forms
    var accountForm = document.getElementById("sign-in-info")
    var signinForm = document.getElementById("sign-up-info");

    // Open the Sign Up page
    openSignUp = () =>{
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation-out");
    overlay.classList.remove("open-sign-in");
    rightText.classList.remove("overlay-text-right-animation");
    // Add classes for animations
    accountForm.className += " form-left-slide-out"
    rightText.className += " overlay-text-right-animation-out";
    overlay.className += " open-sign-up";
    leftText.className += " overlay-text-left-animation";
    // hide the sign up form once it is out of view
    setTimeout(function(){
        accountForm.classList.remove("form-left-slide-in");
        accountForm.style.display = "none";
        accountForm.classList.remove("form-left-slide-out");
    }, 700);
    // display the sign in form once the overlay begins moving right
    setTimeout(function(){
        signinForm.style.display = "flex";
        signinForm.classList += " form-right-slide-in";
    }, 200);
    }

    // Open the Sign In page
    openSignIn = () =>{
        // Remove classes so that animations can restart on the next 'switch'
        leftText.classList.remove("overlay-text-left-animation");
    overlay.classList.remove("open-sign-up");
    rightText.classList.remove("overlay-text-right-animation-out");
    // Add classes for animations
    signinForm.classList += " form-right-slide-out";
    leftText.className += " overlay-text-left-animation-out";
    overlay.className += " open-sign-in";
    rightText.className += " overlay-text-right-animation";
    // hide the sign in form once it is out of view
    setTimeout(function(){
        signinForm.classList.remove("form-right-slide-in")
        signinForm.style.display = "none";
        signinForm.classList.remove("form-right-slide-out")
    },700);
    // display the sign up form once the overlay begins moving left
    setTimeout(function(){
        accountForm.style.display = "flex";
        accountForm.classList += " form-left-slide-in";
    },200);
    }

    // When a 'switch' button is pressed, switch page
    openSignUpButton.addEventListener("click", openSignUp, false);
    openSignInButton.addEventListener("click", openSignIn, false);
</script>
