﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

        
        
		<div class="container">
         
			<div class="row">
				<div class="col-md-5 col-sm-5">
                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/SBILogo_state-bank-of-india-new.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">STATE BANK OF INDIA</h4>
						
                    <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	ANUPAM TOUR AND TRAVELS<br />BANK NAME        :	STATE BANK OF INDIA<br />ACCOUNT NO       :	38100166700<br />IFSC code        :	SBIN0004640</p>
        
                          
						</div>
					</div>
                      
				</div>

             

               

				<div class="col-md-5 col-sm-5">                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Hdfc-Logo.png"" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">HDFC Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	ANUPAM TOUR AND TRAVELS<br />BANK NAME        :	HDFC BANK<br />ACCOUNT NO       :	50200033905690<br />IFSC code        :			HDFC0000531</p>
                           
                            
						</div>
					</div>
                      
				</div>
				<div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Axis-Bank-Logo.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">Axis Bank</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	ANUPAM TOUR AND TRAVELS<br />BANK NAME        :	AXIS BANK<br />ACCOUNT NO       :	918020094925214<br />IFSC code        :			UTIB0001355</p>
						</div>
					</div>
                     
				</div>


                <div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="https://play-lh.googleusercontent.com/75lQsMlFx4fIxbD4OPKAqfqtN9UQZAXKvaK7o5Rtw4MPSVDb7UeULvMuqtdIA4_--kg" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">IndusInd Bank</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	ANUPAM TOUR AND TRAVELS<br />BANK NAME        :	INDUSIND<br />ACCOUNT NO       :	201003298443<br />IFSC code        :			INDB0001431</p>
						</div>
					</div>
                     
				</div>


                <div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="https://financialtribune.com/sites/default/files/styles/360x260/public/field/image/17january/jack-straw88_2.png?itok=ey-h4QJL&c=3063b7d0d2b2eedd412294286f2cfa3b" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">IDBI</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	ANUPAM TOUR AND TRAVELS<br />BANK NAME        :	IDBI BANK<br />ACCOUNT NO       :	1170102000003636<br />IFSC code        :			IBKL0001170</p>
						</div>
					</div>
                     
				</div>
			

            <div class="col-md-5 col-sm-5">
                         <div class="col-md-6">                
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i style="position:absolute;right:0; top:0;"><img src="https://logodownload.org/wp-content/uploads/2019/09/google-pay-logo.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">Google Pay</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">9883577816<br />Transfer amount request must be in SBI Bank</p>
						</div>
					</div>
                     </div>
                       <div class="col-md-6">                
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i style="position:absolute;right:0; top:0;"><img src="https://www.searchpng.com/wp-content/uploads/2018/11/phone-pe.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">Phone Pay</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">9883577816<br />Transfer amount request must be in HDFC Bank</p>
						</div>
					</div>
                     </div>

				</div>
			
				
			</div>

            <hr />

            <p style="font-size:15px; font-weight:600;">Note :</p>
            <p>1. No transactionin ICICI Bank from 1st November 2020.</p>
            <p>2. No request in ICICI Bank, will be accepted after 31st October 2020.</p>
            <p>3. Cash deposite charges remains Rs. 25/- as applicable earlier.</p>
            <p>4. All account name are same.</p>
		</div>

      
     <br />
     <br />
     <br />
        

	</section>



    

</asp:Content>

